if (document.getElementById("doIt")) {
  document.getElementById("doIt").addEventListener("click", postPerson);
} else {
  document.getElementById("putIt").addEventListener("click", putPerson);
}

if (window.location.href.endsWith("?")) {
  window.location.replace("/");
}

function getData() {
let data = {};
  data.firstName = document.getElementById("firstName").value;
  data.secondName = document.getElementById("secondName").value;
  data.occupation = document.getElementById("occupation").value;
  data.gender = document.getElementById("gender").value;
  data.age = parseInt(document.getElementById("age").value);
  if (document.id) {
    data.id = parseInt(document.id.toString());
  }
  return JSON.stringify(data);
}

function fillFields(id) {
fetch("/civilian/" + id).then(response => response.json()).then(data => {
  document.getElementById("firstName").value = data.firstName;
  document.getElementById("secondName").value = data.secondName;
  document.getElementById("occupation").value = data.occupation;
  document.getElementById("gender").value = data.gender;
  document.getElementById("age").value = data.age;
  document.id = id;
})
}

function doPerson(action) {
fetch("/civilian", {
      method: action, // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'include', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: getData() // body data type must match "Content-Type" header
    }).then((response) => {
      response.json().then((json) =>  {
      console.log(json);
        window.location.replace("/person/"+json.id);
      })
    }).catch((error) => {
         document.getElementById("backButton").click();
       });
}

function postPerson() {
  doPerson("POST")
    }

    function putPerson() {
      doPerson("PUT")
        }